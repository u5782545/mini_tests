import java.util.Scanner;

/**
 * Created by u5782545 on 1/03/16.
 */
public class Fibonacci {
    private static int Fibonacci(int n){
        if (n == 0){
            return 0;
        }
        else if (n == 1)
            return 1;
        else {
            return Fibonacci(n-1)+Fibonacci(n-2);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter an integer larger than 2:");
        int n = in.nextInt();
        while (n < 2){
            System.out.println("Please enter an integer larger than 2:");
            n = in.nextInt();
        }

        for (int i = 0; i < n; i++){
            System.out.print(i == n-1 ? Fibonacci(i):Fibonacci(i)+", ");
        }
    }
}
